Hide entity id in widget autocomplete for field type entity_reference.

Category:
 - UX
 - Form

Hooks used:
 - hook_menu_alter()
 - hook_field_widget_form_alter()
